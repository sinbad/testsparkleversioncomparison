//
//  SSAppDelegate.h
//  TestSparkleVersionComparison
//
//  Created by Steven Streeting on 07/02/2014.
//  Copyright (c) 2014 Steven Streeting. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SSAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (copy) NSString* versionA;
@property (copy) NSString* versionB;
@property (copy) NSString* result;

@end
