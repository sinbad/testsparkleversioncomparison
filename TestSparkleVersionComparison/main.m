//
//  main.m
//  TestSparkleVersionComparison
//
//  Created by Steven Streeting on 07/02/2014.
//  Copyright (c) 2014 Steven Streeting. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
